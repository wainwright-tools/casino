#!/bin/bash

curl 'https://laravel.initializer.dev/create-project' \
  -H 'authority: laravel.initializer.dev' \
  -H 'accept: text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.9' \
  -H 'accept-language: en-ZA,en;q=0.9' \
  -H 'cache-control: no-cache' \
  -H 'content-type: application/x-www-form-urlencoded' \
  -H 'cookie: laravel_session=eyJpdiI6IlYzZzlUY2ZZMjRWUEtqZlR0R2xlekE9PSIsInZhbHVlIjoiaW5XbGJaSFY2eHhCSW5HZ1UzblNncWgyV2RJZWRxSWNtT3kvT3FLNElxMFIvMjFRVlJBK0VDSDFKT2ljd1g0ZHhva3gvZU45eVZOZmJ6Zi9menhLUElZVjNHTWpzbTFzV1VIUVVkbVRxd3crcHBjYWRnMGo0TmNVdmFKRDk5dHoiLCJtYWMiOiJiNTc5YjVjMzMyMmU5M2MxZjVjMDQ4NGQzYTNjM2VmNTcwNGRjMGQ2YzZmNWY2NDM1OWQwNTNkN2E0NWFmMjQ5IiwidGFnIjoiIn0%3D' \
  -H 'origin: https://laravel.initializer.dev' \
  -H 'pragma: no-cache' \
  -H 'referer: https://laravel.initializer.dev/' \
  -H 'sec-ch-ua: ".Not/A)Brand";v="99", "Google Chrome";v="103", "Chromium";v="103"' \
  -H 'sec-ch-ua-mobile: ?0' \
  -H 'sec-ch-ua-platform: "Linux"' \
  -H 'sec-fetch-dest: document' \
  -H 'sec-fetch-mode: navigate' \
  -H 'sec-fetch-site: same-origin' \
  -H 'sec-fetch-user: ?1' \
  -H 'upgrade-insecure-requests: 1' \
  -H 'user-agent: Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/103.0.5060.114 Safari/537.36' \
  --data-raw 'vendor=wainwright-casino-tools&project=laravel&php=8.1&description=Base+of+your+casino+stack&breeze-frontend=blade&jetstream-frontend=livewire&starter=jetstream&database=mariadb&uses-dbal=uses-dbal&uses-minio=minio&cache=memcached&queue=redis&mail=none&broadcasting=none&scout=none&uses-telescope=uses-telescope&cashier=none' \
  --compressed \
  --output lar/laravel.zip