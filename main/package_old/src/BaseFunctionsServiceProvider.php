<?php

namespace Respins\BaseFunctions;

use Spatie\LaravelPackageTools\Package;
use Spatie\LaravelPackageTools\PackageServiceProvider;
use Respins\BaseFunctions\Commands\BaseFunctionsCommand;
use Respins\BaseFunctions\ProxyHelper;
use Illuminate\Contracts\Http\Kernel;
use Livewire\Livewire;

class BaseFunctionsServiceProvider extends PackageServiceProvider
{
    public function configurePackage(Package $package): void
    {   
        //Register package functions
        $package
            ->name('base-functions')
            ->hasConfigFile(['baseconfig', 'adminconfig', 'gameconfig'])
            ->hasViews('respins')
            ->hasRoutes('base', 'game')
            ->hasMigrations(['create_metadata_table', 'create_gamesessions_table', 'create_players_table', 'create_rawgameslist_table', 'create_datalogger_table', 'create_operatoraccess_table', 'create_gameslist_table']);

            //Register the proxy
            $this->app->bind('ProxyHelper', function($app) {
                return new ProxyHelper();
            });

            //$this->app->router->pushMiddlewareToGroup('web', \Respins\BaseFunctions\Middleware\RespinsIPCheck::class);
            //$this->app->router->pushMiddlewareToGroup('api', \Respins\BaseFunctions\Middleware\RespinsIPCheck::class);
            $this->loadLivewireComponents();

     }


    private function loadLivewireComponents()
    {
        Livewire::component('navigation-bar', \Respins\BaseFunctions\Controllers\Livewire\NavigationBar::class); 
        Livewire::component('games-launcher', \Respins\BaseFunctions\Controllers\Livewire\GamesLauncher::class); 
        Livewire::component('games-list', \Respins\BaseFunctions\Controllers\Livewire\GamesList::class); 
        Livewire::component('maintenance-clear-cache', \Respins\BaseFunctions\Controllers\Livewire\MaintenancePanel::class); 
        Livewire::component('user-datatable', \Respins\BaseFunctions\Controllers\Livewire\Partials\UserDataTable::class); 
        Livewire::component('gamesessions-datatable', \Respins\BaseFunctions\Controllers\Livewire\Partials\GameSessionsDataTable::class); 
        Livewire::component('gameslist-datatable', \Respins\BaseFunctions\Controllers\Livewire\Partials\GamesListDataTable::class); 
        Livewire::component('operatorkeys-datatable', \Respins\BaseFunctions\Controllers\Livewire\Partials\OperatorKeysDataTable::class); 
        Livewire::component('operator-panel', \Respins\BaseFunctions\Controllers\Livewire\OperatorPanel::class); 
        Livewire::component('mock-panel', \Respins\BaseFunctions\Controllers\Livewire\MockPanel::class); 


    }
}

 
