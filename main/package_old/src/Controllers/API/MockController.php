<?php
namespace Respins\BaseFunctions\Controllers\API;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Log;

class MockController
{

    public static function command_center($command, $data = NULL) 
    {
        $player = new MockController;
        if($command === 'refreshMockPlayer') 
        {
            Cache::pull('active_mock_player');
            $profile = $player->getActiveMockPlayer();
            return collect($profile);
        }

        if($command === 'activeMockPlayer')
        {
            $profile = $player->getActiveMockPlayer();
            return collect($profile);
        }

        if($command === 'selectMockPlayerById')
        {
            $profile = $player->getActiveMockPlayer();
            return collect($player->selectMockPlayerById($data['mock_player_id']));
        }

        return 'error';
    }

    public function mockRouter(Request $request)
    {
        Log::debug($request);

        $action = $request->header('x-respins-action') ?? NULL;
        if($action === 'ping') {
              return response()->json(array('status' => 'pong'), 202);
        }
        if($action === 'balance') {
              return response()->json(array('status' => 'success', 'balance' => 10000));
        }
        if($action === 'game') {
              return response()->json(array('status' => 'success', 'balance' => 99999, 'received' => $request->all()));
        }
        return response()->json(array('ip' => '81.24.11.236', 'received' => $request->all()));
    }

    public function createMockPlayer()
    {
        $player_name = self::generateRandomName();
        $player_profile = [
            'mock_player_id' => $player_name,
            'balance' => (int) config('baseconfig.mock.starting_balance') ?? 10000,
            'currency' => config('baseconfig.mock.default_currency') ?? 'USD',
            'net_win' => 0,
            'net_loss' => 0, 
        ];
        $store_cache = Cache::put('mock_player:'.$player_name, $player_profile);
        if($store_cache === 0) {
            $message = array('status' => 'error', 'data' => 'Error storing mock player in cache. The mock module runs completely in cache, check if cache settings are correct.');
            return response()->json($message, 400);
        }
        $message = array('status' => 'success', 'data' => $player_profile);
        return $message;
    }

    public function updateMockPlayer($mock_player_id, $key, $value)
    {  
        $select = $this->selectMockPlayerById($mock_player_id);
        if($select) {
           $select[$key] = $value;
           $store_in_cache = Cache::put('mock_player:'.$mock_player_id, $select);
           $message = array('status' => 'success', 'data' => $select);
           return $message;
        }
    }

    public function selectMockPlayerById($mock_player_id)
    {
        $get = Cache::get('mock_player:'.$mock_player_id);
        if(!$get) {
            return false;
        }
        $message = array('status' => 'success', 'data' => $get);
        return $message;
    }

    public function getActiveMockPlayer()
    {
        $active_player = Cache::get('active_mock_player');
        if(!$active_player) {
            $create_player = $this->createMockPlayer();
            $active_player = $create_player['data'];
            $store_cache = Cache::put('active_mock_player', $active_player);
            if($store_cache === 0) {
                $message = array('status' => 'error', 'data' => 'Error storing active player in cache. The mock module runs completely in cache, check if cache settings are correct.');
                return $message;
            }
        }
        return $active_player;
    }

    public static function generateRandomName()
    {
        $names = array(
            'dejan-jovic',
            'honest-matteo',
            'ivan-montik',
            'tiger-dalian',
            'david-wainwright',
            'vlad-graphql',
            'laurence-voc',
            'iztok-nice-guy',
            'uri-lepra',
            'rux-nelson',
            'mr-bit',
            'god-mladen',
            'dix-china',
            'jan-joven-jumao',
            'erik-cas',
            'marvin-vallea',
            'zhack-01',
            'riann-draft',
            'jeff-norte-barry',
        );
        $random_name = $names[rand(0, count($names) - 1)]; 
        return $random_name.rand(100, 9999);
    }
}

