<?php
namespace Respins\BaseFunctions\Controllers;

use Illuminate\Support\Facades\Cache;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Http;
use DB;
use Respins\BaseFunctions\BaseFunctions;
use Respins\BaseFunctions\Traits\ApiResponseHelper;
use Respins\BaseFunctions\Models\RawGameslist;
use Respins\BaseFunctions\Models\Gameslist;
use Respins\BaseFunctions\Models\WhitelistIPs;
use Respins\BaseFunctions\Models\DataLogger;
use Laravel\Dusk\Chrome;
use Illuminate\Support\Facades\Log;

class DataController
{
    use ApiResponseHelper;

    public static function retrieveOriginDemoLink($gid)
    {
        $select = Gameslist::where('gid', $gid)->first();
        $get = Http::timeout(6)->get('https://www.'.$select['source'].$select['demolink']);
        $origin_demo_launch = BaseFunctions::in_between('{\"game_url\":\"', '\",\"strategy\"', $get);
        $back_slash_removal = BaseFunctions::remove_back_slashes(urldecode($origin_demo_launch)); //remove backslashes
        $final_url = str_replace("u0026", "&", $back_slash_removal);
        $select->update([
            'demolink' => $final_url
        ]);
    }

    public static function logger($type, $data, $extra_data = NULL) 
    {
            DataLogger::save_log($type, $data, $extra_data);
    }

    public static function getRedirectUrl($url) {
        $headers = get_headers($url, 1);
        if ($headers !== false && isset($headers['Location'])) {
            $final_url = is_array($headers['Location']) ? array_pop($headers['Location']) : $headers['Location'];
            return $final_url;
        }
        return false;
    }

    public static function extra_data_gameslist($gid)
    {
        Gameslist::where('gid', $gid)->first();
        $request_game_session = $game_controller::requestSession($final_session_data);

    }

    public static function check_casino_eligible($url)
    {
        if (filter_var($url, FILTER_VALIDATE_URL) === FALSE) {
            $final_data = array('job_state' => 'failed', 'url_check' => $url);
            DataLogger::save_log('casino_check', $final_data);
        } else {

        try {
            $replace_url = str_replace("\n", " ", $url);
            $replace_url_2 = str_replace("\t", " ", $replace_url);
            $final_url = urldecode($replace_url_2);
            $getRedirect = self::getRedirectUrl($final_url);
            if($getRedirect === false) {
            Log::debug('Invalid final url:'.$final_url);
            } else {
                $api_url = 'https://'.$getRedirect.'/api/games/allowed_desktop';
                $get = Http::timeout(4)->get($api_url);
                $final_data = array('job_state' => 'success', 'url_check' => $url, 'redirect_url' => $getRedirect, 'api_response' => json_decode($get, true));
                if(json_decode($get, true) !== NULL) {            
                    Log::critical('success finding api '.$api_url);
                }
                DataLogger::save_log('casino_check', $final_data);
                echo $final_data;
            }
        } catch (\Exception $exception) {
          //$final_data = array('job_state' => 'failed', 'url_check' => $url, 'reason' => $exception);
          //DataLogger::save_log('casino_check', $final_data);
        }
        }
    }



    public static function mr_gambled_casino_source()
    {
        $url = "https://mr-gamble.com/en/api/rpc/getAllCasinosUsingParamsApi/";

        $curl = curl_init($url);
        curl_setopt($curl, CURLOPT_URL, $url);
        curl_setopt($curl, CURLOPT_POST, true);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);

        $headers = array(
        "authority: mr-gamble.com",
        "accept: */*",
        "accept-language: en-ZA,en;q=0.9",
        "anti-csrf: 57SQje-0mEKifJYT3Ixr6CwFS0EvgT8h",
        "cache-control: no-cache",
        "content-type: application/json",
        "cookie: _gid=GA1.2.1521553994.1660017151; mrg_sPublicDataToken=eyJ1c2VySWQiOm51bGx9; mrg_sAnonymousSessionToken=eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJibGl0empzIjp7ImlzQW5vbnltb3VzIjp0cnVlLCJoYW5kbGUiOiI4SWw3bjZGM2RGQS1tQ0NZeHp1dHgwVFRoc213alFWOTphand0IiwicHVibGljRGF0YSI6eyJ1c2VySWQiOm51bGx9LCJhbnRpQ1NSRlRva2VuIjoiNTdTUWplLTBtRUtpZkpZVDNJeHI2Q3dGUzBFdmdUOGgifSwiaWF0IjoxNjYwMDE3MTQ5LCJhdWQiOiJibGl0empzIiwiaXNzIjoiYmxpdHpqcyIsInN1YiI6ImFub255bW91cyJ9.7J5EAyXPXK2mw9IVHTXYUBX4qN1xoB8Am2dzODKx6Eg; mrg_sAntiCsrfToken=57SQje-0mEKifJYT3Ixr6CwFS0EvgT8h; _ga_XMRJNYKN0E=GS1.1.1660017151.1.1.1660017538.0; _gat_UA-149010969-1=1; _ga_7Y478BS5VQ=GS1.1.1660017151.1.1.1660017538.0; _ga=GA1.1.1607322067.1660017151",
        "origin: https://mr-gamble.com",
        "pragma: no-cache",
        "referer: https://mr-gamble.com/en/",
        "sec-ch-ua-mobile: ?0",
        "sec-fetch-dest: empty",
        "sec-fetch-mode: cors",
        "sec-fetch-site: same-origin",
        "user-agent: Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/103.0.5060.114 Safari/537.36",
        );
        curl_setopt($curl, CURLOPT_HTTPHEADER, $headers);
        $data = '{"params":{"pageNumber":45,"param":{"pageNumber":45},"quickFilter":"a","country":{"id":"","name":""}},"meta":{}}';
        curl_setopt($curl, CURLOPT_POSTFIELDS, $data);

        //for debug only!
        curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, false);
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);

        $resp = curl_exec($curl);
        curl_close($curl);
        $decoded_result = json_decode($resp, true);
        $batch = 'mr-gamble-1';

        foreach($decoded_result['result'] as $result) { 
            $data = array('casino_name' => $result['name'], 'casino_id' => $result['affiliateMask'], 'casino_link' => 'https://mr-gamble.com/en/go/'.$result['affiliateMask']);
            DataLogger::save_log($batch, $final_data);
        }
    }

    public static function generate_pragmatic_real_token() {
        $url = "https://api.atlantgaming.com/api/v2/launcher";
        $curl = curl_init($url);
        curl_setopt($curl, CURLOPT_URL, $url);
        curl_setopt($curl, CURLOPT_POST, true);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        $headers = array(
            "authority: api.atlantgaming.com",
            "accept: */*",
            "accept-language: en-ZA,en;q=0.9",
            "cache-control: no-cache",
            "content-type: text/plain;charset=UTF-8",
            "origin: https://launch.atlantgaming.com",
            "pragma: no-cache",
            "referer: https://launch.atlantgaming.com/",
            "sec-ch-ua-mobile: ?0",
            "sec-fetch-dest: empty",
            "sec-fetch-mode: cors",
            "sec-fetch-site: same-site",
            "user-agent: Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/103.0.5060.114 Safari/537.36",
        );
        curl_setopt($curl, CURLOPT_HTTPHEADER, $headers);
        $data = '{"jsonrpc":"2.0","params":{"token":"r-8cbfdccd302d69430d0aa7ac7eb5e4"},"method":"sessions/launch"}';
        curl_setopt($curl, CURLOPT_POSTFIELDS, $data);
        //for debug only!
        curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, false);
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
        $resp = curl_exec($curl);
        curl_close($curl);
        $decoded_resp = json_decode($resp, true);
        $final_url = self::getRedirectUrl($decoded_resp['data']['url']);
        $hit = self::getRedirectUrl($final_url);
        parse_str($final_url, $q_arr);
        return 'https://aventonv-dk1.pragmaticplay.net/gs2c/promo/active/?symbol=vs25wolfgold&mgckey='.$q_arr['mgckey'];
    }

    public static function mr_gamble_casino_source()
    {
        $url = "https://list.casino/page-data/online-casinos/page-data.json";
        $curl = curl_init($url);
        curl_setopt($curl, CURLOPT_URL, $url);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        //for debug only!
        curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, false);
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
        $resp = curl_exec($curl);
        curl_close($curl);
        $decoded_result = json_decode($resp, true);
        $batch = 'mr-gamble-1';

        foreach($decoded_result['result']['data']['contentfulWebsitePage']['components'] as $result) {
            if($result['id'] === 'b6899eed-cc8b-503c-b928-dea5d62208ee') {

                foreach($result['casinosList'] as $casino) {
                    self::check_casino_eligible($casino['referralUrl']);
                    echo $casino['referralUrl'];
                }
            }
            //$data = array('casino_name' => $result['name'], 'casino_id' => $result['affiliateMask'], 'casino_link' => 'https://mr-gamble.com/en/go/'.$result['affiliateMask']);
            //DataLogger::save_log($batch, $final_data);
        }
    }

    public static function getProviders()
    {   
        $cache_length = config('baseconfig.caching.length_getProvider');
        if($cache_length === 0) {
            return Gameslist::Providers();
        }

        $value = Cache::remember('cache:getProviders', $cache_length, function () {
            return Gameslist::Providers();
        });
        return $value;
    }

    public static function getWhitelistIPs()
    {   
        $cache_length = config('baseconfig.caching.length_getWhitelistIPs');
        if($cache_length === 0) {
            return collect(WhitelistIPs::collect_ips());
        }

        $value = Cache::remember('cache:getWhitelistIPs', $cache_length, function () {
            return collect(WhitelistIPs::collect_ips());
        });
        return $value;
    }

    public static function getGames()
    {   
        $cache_length = config('baseconfig.caching.length_getGames');
        
        if($cache_length === 0) {
            return Gameslist::all();
        }
        $value = Cache::remember('raw:getGames', $cache_length, function () {
            return Gameslist::all();
        });

        return $value;
    } 

    public static function selectProvider($provider = NULL)
    {
        if($provider === NULL) {
            return false;
        }

        $retrieve = self::getProviders();
        if($retrieve === NULL) { 
            return false;
        }
        $collection = collect(json_decode($retrieve));
        $select_provider = $collection->where('slug', $provider)->first();

        if($select_provider !== NULL) {
            $merge = array('count' => self::$model_games);
            $m = $select_provider->merge($merge);
            return $m;
        } else {
            return false;
        }
    }

}
