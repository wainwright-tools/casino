<?php
namespace Respins\BaseFunctions\Controllers\Game;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Contracts\Support\Arrayable;
use Respins\BaseFunctions\Controllers\API\SessionController;
use Illuminate\Support\Facades\Http;
use Respins\BaseFunctions\BaseFunctions;
use Illuminate\Support\Facades\Log;

class EvoplayController extends SessionController
{
    public function evoplayCallbackEndpoint(Request $request)
    {
        $action = $request->header('x-respins-action') ?? NULL;
        if($action === 'ping') {
              return response()->json(array('status' => 'pong'), 202);
        }
        if($action === 'balance') {
              return response()->json(array('status' => 'success', 'balance' => 10000));
        }
        if($action === 'game') {
              return response()->json(array('status' => 'success', 'balance' => 99999, 'received' => $request->all()));
        }
        return response()->json(array('ip' => '81.24.11.236', 'received' => $request->all()));
    }

    public static function evoplay_gameid_transformer($game_id, $direction)
    { 
        if($direction === 'explode') {
            try {
                $explode_game = explode('/', $game_id);
                $exploded_game_id = $explode_game[1];
                return $exploded_game_id;
            } catch (\Exception $exception) {
                Log::warning('Errored trying to transform & explode game_id on bgaming_gameid_transformer() function in bgamingcontroller.');
                return false;
            }
        } elseif($direction === 'concat') {
            $concat = 'evoplay/'.$game_id;
            return $concat;
        }
        Log::warning('Transform direction not supported, use concat or explode on evoplay_gameid_transformer().');
        return false;
    }

    public static function requestSession($session = NULL)
    {
        $proposed_session = $session; // validate this again if you multi-server setup between API & actual session creation jobs
        $select_session = SessionController::sessionData($proposed_session['token_internal']);
        if($select_session === false or !$select_session['session_data']) { //internal session not found
               return false;
        }
        $player_id = $select_session['session_data']['player_id'];
        $currency = $select_session['session_data']['currency'];
        $token_internal = $select_session['session_data']['token_internal'];
        $game_id = self::evoplay_gameid_transformer($select_session['session_data']['game_id_original'], 'explode');
        
        $api_endpoint = config('gameconfig.evoplay.endpoint');
        $api_endpoint_key = config('gameconfig.evoplay.key');
        $api_final_url = $api_endpoint.'?game='.$game_id.'&mode=real&player='.$player_id.'&currency='.$currency;

        $request = Http::timeout(5)->withHeaders([
            'x-respins-key' => $api_endpoint_key,
            'x-respins-action' => 'create_session',
        ])->get($api_final_url);
        return $replaceAPItoOurs;
    }

    public static function curl($url)
    {
        $curl = curl_init($url);
        curl_setopt($curl, CURLOPT_URL, $url);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, false);
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
        $httpcode = curl_getinfo($curl, CURLINFO_HTTP_CODE);
        $resp = curl_exec($curl);
        curl_close($curl);
        return json_decode($resp, true);
    }
}