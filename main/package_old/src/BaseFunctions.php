<?php

namespace Respins\BaseFunctions;
use Respins\BaseFunctions\Models\Players;
use Respins\BaseFunctions\Models\OperatorAccess;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Str;
use Illuminate\Http\Request;
use DB;
use Respins\BaseFunctions\Models\Gameslist;
use Illuminate\Support\Facades\Http;
use Respins\BaseFunctions\Controllers\DataController;
class BaseFunctions
{
    public static function requestIP(Request $request) 
    {
        $ip = $request->header('CF-Connecting-IP');
        if($ip === NULL || !$ip) { 
            $ip = $_SERVER['REMOTE_ADDR'];
            if($ip === NULL) {
              $ip = $request->ip();  
            }
        }
        return $ip;
    }
    
    public static function remove_back_slashes($string)
    {
        $string=implode("",explode("\\",$string));
        return stripslashes(trim($string));
    }

    public static function follow_redirect($url) 
    {

    }

    public static function scrape_urls_from_page(Request $request)
    {
    return DataController::mr_gamble_casino_source();

    $var = self::fread_url($request->url);

    preg_match_all ("/a[\s]+[^>]*?href[\s]?=[\s\"\']+"."(.*?)[\"\']+.*?>"."([^<]+|.*?)?<\/a>/",  $var, $matches);

    $matches = $matches[1];
    $list = array();

    foreach($matches as $var)
    {    

        if (filter_var($var, FILTER_VALIDATE_URL) === FALSE) {
            //action on malformed url format
        } else {
            DataController::check_casino_eligible($var);
        }
    }

    }

    public static function fread_url($url,$ref="")
    {
        if(function_exists("curl_init")){
            $ch = curl_init();
            $user_agent = "Mozilla/4.0 (compatible; MSIE 5.01; ".
                          "Windows NT 5.0)";
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_USERAGENT, $user_agent);
            curl_setopt( $ch, CURLOPT_HTTPGET, 1 );
            curl_setopt( $ch, CURLOPT_RETURNTRANSFER, 1 );
            curl_setopt( $ch, CURLOPT_FOLLOWLOCATION , 1 );
            curl_setopt( $ch, CURLOPT_FOLLOWLOCATION , 1 );
            curl_setopt( $ch, CURLOPT_URL, $url );
            curl_setopt( $ch, CURLOPT_REFERER, $ref );
            curl_setopt ($ch, CURLOPT_COOKIEJAR, 'cookie.txt');
            $html = curl_exec($ch);
            curl_close($ch);
        }
       else{
            $hfile = fopen($url,"r");
            if($hfile){
                while(!feof($hfile)){
                    $html.=fgets($hfile,1024);
                }
            }
        }
        return $html;
    }
    public static function internal_apikey()
    {
        $internal_key = Cache::get('internal_apikey');
        if(!$internal_key) {
            $search_key = OperatorAccess::where('operator_access', 'internal')->first();

            if(!$search_key) {
            $data = [
                'operator_access' => 'internal',
                'callback_url' => config('baseconfig.mock.callback_url'),
                'active' => 1,
                'ownedBy' => 1,
            ]; 
            $create_key = \Respins\BaseFunctions\Controllers\API\OperatorController::createOperatorKey($data);
            $search_key = json_decode($create_key, true);
            } 
            $internal_key = $search_key['operator_key'];
            Cache::put('internal_apikey', $internal_key);
        }
        return $internal_key;
    }
    public static function testingRedirect(Request $request)
    {
        $controller = $request->controller;
        $class = $request->class;
        $prefix_class = $request->prefix_class;
        if($prefix_class !== 'Models') {
            if($prefix_class !== 'Controllers') {
                return 'Either use Models or Controllers in prefix_class';
            }
        }
        $controller = '\Respins\BaseFunctions\\'.$prefix_class.'\\'.$controller.'::'.$class;
        return $controller();
        return $controller;
    }

    // Helper intended to parse "in_between" values mainly for html content (resource intensive)
    public static function in_between($a, $b, $data) 
    {
        preg_match('/'.$a.'(.*?)'.$b.'/s', $data, $match);
        if(!isset($match[1])) {
            return false;
        }
        return $match[1];
    }

    // Generating security signature - using hmac signing, you can change algo's however MD5 is fastest
    // https://www.php.net/manual/en/function.hash-hmac.php
    public static function generate_sign($token, $pwd = NULL) 
    {
        $timestamp = time(); 
        if($pwd === NULL) {
            $pwd = config('gameconfig.api_settings.signature_password');
        }
        $encryption_key = $pwd.'-'.$timestamp; //Consider timestamp the randomizing salt, can be replaced by any randomizing key/regex
        $generate_sign = hash_hmac('md5', $token, $encryption_key);
        $concat_sign_time = $generate_sign.'-'.$timestamp;
        return $concat_sign_time;
    }

    public static function verify_sign($signature, $token, $pwd = NULL)
    {
        if($pwd === NULL) {
            $pwd = config('gameconfig.api_settings.signature_password');
        }
        try {
            $explode_signature = explode('-', $signature); 
            $timestamp = $explode_signature[1];
            $encryption_key =  $pwd.'-'.$timestamp;
            $generate_sign = hash_hmac('md5', $token, $encryption_key);
            $concat_sign_time = $generate_sign.'-'.$timestamp;
            if($signature === $concat_sign_time) { // verify signature is same outcome
                return true;
            }
        } catch (\Exception $exception) {
            return false;
        }
        return false; //signature not matching, returning false
    }

    public static function evoplayKeyset()
    {
        $get = Http::retry(1, 5000)->withHeaders(['x-respins-callback' => 'https://web.bog.asia/api/respins.io/aggregation/callback/evoplay'])->get('http://u140512p136552.web0121.zxcs-klant.nl/api/games/key');
        return $get;
    }

    public static function messageHelper($message)
    {
        $message = array('message' => $message);
        return $message;
    }
    
    public static function responseOk(?array $data = null)
    {
        $data ??= [];
        $data = self::morphToArray($data);
        $response = array(
            "state" => "Ok",
            "data" => $data, 
            "code" => 200,
        );
        return response()->json($response, 200);
    }
    
    public static function responseError(?array $data = null)
    {
        $data ??= [];
        $data = self::morphToArray($data);
        $response = array(
            "state" => "Ok",
            "data" => $data, 
            "code" => 400,
        );
        return response()->json($response, 400);
    }

    public static function helperGamelink() {
        if(config('baseconfig.frontend.launcher_location') === 'local') {
            return '/';
        } else {
            return config('baseconfig.frontend.launcher_location');
        }
    } 

    public static function davidkohenCollect()
    {
        //$db = DB::unprepared(file_get_contents('/laravel/laravel/surf/livewire-only/example-app/public/db_games.sql'));
        //Artisan::call('migrate:reset', ['--force' => true]);
        return Gameslist::collectify_davidkohen_list();
    }

    public static function migrate() 
    {
        $migrate = \Artisan::call('migrate:fresh');
        $cache_clear = \Artisan::call('cache:clear');
        return $cache_clear;
    } 

    public static function morphToArray($data)
    {
        if ($data instanceof Arrayable) {
            return $data->toArray();
        }
        if ($data instanceof JsonSerializable) {
            return $data->jsonSerialize();
        }
        return $data;
    }


    # Error routing
    # Usage Example: return \Respins\BaseFunctions\BaseFunctions::errorRouting(401, 'Failed to create player.');
    public static function errorRouting($statuscode, $message = NULL, $errorType = NULL, $data = NULL)
    {
        //Array with meta
        if($message !== NULL) {
            $message = array(
                'status' => $statuscode,
                'message' => $message,
                'type' => $errorType,
                'data' => $data,
            );
        } else {
                $message = array(
                'status' => $statuscode,
                'message' => $message,
                'type' => $errorType,
                'data' => $data,
                );
        }
        #Operator Error Page
        // Operator level error page (casino)
        if($errorType === 'operator') {
            return view('respins::error-operator-template')->with('error', $message);
        }
        #Game Provider Error Page
        // Per game provider erroring
        if($errorType === 'gameprovider') {
            return view('respins::error-gameprovider-template')->with('error', $message);
        }
        #Fallback Error Page
        // Error page that is used if nothing is used
        return view('respins::error-default-template')->with('error', $message);
    }
}



