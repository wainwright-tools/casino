<?php

namespace Respins\BaseFunctions\Models;

use \Illuminate\Database\Eloquent\Model as Eloquent;

class MetaData extends Eloquent  {
    protected $table = 'respins_metadata';
    protected $timestamp = true;
    protected $primaryKey = 'id';

    protected $fillable = [
        'key',
        'type',
        'value',
        'extended_key',
    ];
    protected $casts = [
        'active' => 'boolean',
        'object_data' => 'json',
        'created_at' => 'datetime',
        'updated_at' => 'datetime',
    ];

}