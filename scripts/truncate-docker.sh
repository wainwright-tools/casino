#!/bin/bash
docker info > /dev/null 2>&1

# Ensure that Docker is running...
if [ $? -ne 0 ]; then
    echo "Docker is not running."
    exit 1
fi

read -p "Are you sure you want to remove all docker images, docker containers, prune docker system & recreating main network? <y/N> " prompt
if [[ $prompt =~ [yY](es)* ]]; then
    echo "Stopping containers.."
    sleep 5
    docker stop $(docker ps -a -q)
    echo "Pruning docker containers & images.."
    sleep 5
    docker rm $(docker ps -a -q)
    echo "Removing all docker images.."
    sleep 5
    docker rmi $(docker images -a -q) -f
    echo "Pruning docker system.."
    sleep 5
    docker system prune -f
    echo "Recreating main network (ipv6 disabled).."
    sleep 5
    docker network create --ipv6=false main_network
    exit 1
fi
